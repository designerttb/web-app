import logo from './assets/img/logo-react-gray.png';
import bg from './assets/img/2.jpg'
import './App.css';
import Home from './components/Home.js'
import {BrowserRouter as Router,
Route,
Link} from 'react-router-dom'



function App() {
  return (
    
    <div className="App">
      
        <header className="App-header">
        
        </header>

        <div id="main">
        <div id="leftMenu">
            <div className="logo">
              <img className="nav-logo" src={logo} alt="react"></img>
            </div>
            <ul id="nav">  
              <li><i className="icon"></i><a>Home</a></li>
              <li><i className="icon"></i><a>App 1</a></li>
              <li><i className="icon"></i><a>App 2</a></li>
              <li><i className="icon"></i><a>App 3</a></li>
              <li><i className="icon"></i><a>App 4</a></li>
              <li><i className="icon"></i><a>App 5</a></li>
            </ul>         
          </div>
          <div id="content">

          </div>

        </div>      
    </div>
  );
}

export default App;
